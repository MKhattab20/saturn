package com.nasaspaceapps.saturn.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Environment;

import org.apache.commons.io.IOUtils;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;

public class FileUtilities {

    public static boolean copyFile(final File src, final File dst)
            throws IOException {
        if (src.getAbsolutePath().toString()
                .equals(dst.getAbsolutePath().toString()))
            return true;
        else {
            final InputStream is = new FileInputStream(src);
            copyFile(is, dst);
        }
        return true;
    }

    public static void copyFile(final InputStream is, final File dst)
            throws IOException {

        final OutputStream os = new FileOutputStream(dst);
        final byte[] buff = new byte[1024];
        int len;
        while ((len = is.read(buff)) > 0) {
            os.write(buff, 0, len);
        }
        is.close();
        os.close();

    }

    public static void deleteFile(File dir, String fileName) {
        File file = new File(dir, fileName);
        if (file.exists())
            file.delete();
    }

    public static void deleteFile(String path) {
        try {
            File file = new File(path);
            if (file.exists())
                file.delete();
        } catch (Exception e) {
        }
    }

    public static boolean fileIsExits(String filePath) {
        if (filePath != null) {
            File file = new File(filePath);
            if (file.exists())
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    public static void deleteFolder(final File folder) {
        final File[] files = folder.listFiles();
        if (files != null) { // some JVMs return null for empty dirs
            for (final File f : files) {
                if (f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }

    public static File getSDDir(Activity context) {

        return Environment.getExternalStorageDirectory();

    }

    public static String loadRawFile(final Context context, final int resID) {
        try {
            final Resources res = context.getResources();
            final InputStream in_s = res.openRawResource(resID);

            final byte[] b = new byte[in_s.available()];
            in_s.read(b);
            return new String(b);
        } catch (final Exception e) {
            return null;
        }
        // return res.getString(R.string.objidata);
    }

    public static BufferedOutputStream getFileOutputStream(
            final String fileName, String folderName) {
        final File root = Environment.getExternalStorageDirectory();
        final File outDir;
        final File outputFile;
        if (folderName == null) {
            outDir = new File(root.getAbsolutePath(), "APP Files");
            outDir.mkdirs();
        } else {
            outDir = new File(folderName);
            if (outDir.mkdirs()) {
                LogUtil.i("folder creation ", "directory is created");
            }
        }
        try {

            outDir.mkdirs();
            outputFile = new File(outDir, fileName);

            LogUtil.i("Files Writer",
                    "Creating new file : " + outputFile.getAbsolutePath());

            outputFile.createNewFile();
            if (outputFile.exists() && outputFile.length() <= 0) {
                BufferedOutputStream bos = new BufferedOutputStream(
                        new FileOutputStream(outputFile));
                return bos;
            }
        } catch (final IOException e) {
            e.printStackTrace();
            LogUtil.d("Files Writer",
                    "Unable to write file to external storage");
        }
        return null;
    }

    public static void write(final String fileName, final byte[] data,
                             String folderName) {

        try {

            BufferedOutputStream bos = getFileOutputStream(fileName, folderName);
            if (bos != null) {

                bos.write(data);
                bos.flush();
                bos.close();
            }

        } catch (final IOException e) {
            e.printStackTrace();
            LogUtil.d("Files Writer",
                    "Unable to write file to external storage");
        }
    }

    public static void createAppFolder() {
        final File root = Environment.getExternalStorageDirectory();
        final File outDir;
        outDir = new File(root.getAbsolutePath(), "APP Files");

        outDir.mkdirs();

    }

    public static void deleteSerializedData(Context context) {
        final File root = context
                .getDir("serializedData", Context.MODE_PRIVATE);
        deleteFolder(root);
    }

    public static void writeSerializedData(Context context,
                                           final String fileName, final String data) {

        final File root = context
                .getDir("serializedData", Context.MODE_PRIVATE);
        final File outDir = new File(root.getAbsolutePath() + File.separator
                + "APP Files");
        if (!outDir.isDirectory()) {
            outDir.mkdir();
        }
        try {
            if (!outDir.isDirectory())
                throw new IOException(
                        "Unable to create directory . Maybe the SD card is mounted?");
            final File outputFile = new File(outDir, fileName);
            if (outputFile.exists()) {
                outputFile.delete();
                outputFile.createNewFile();
            }
            Writer writer;
            writer = new BufferedWriter(new FileWriter(outputFile));
            writer.write(data);
            LogUtil.d("Files Writer", "Report successfully saved to: "
                    + outputFile.getAbsolutePath());
            writer.close();
        } catch (final IOException e) {
            e.printStackTrace();
            LogUtil.d("Files Writer",
                    "Unable to write file to external storage");
        }
    }

    public static String readSerializedData(Context context,
                                            final String fileName) {
        final File root = context
                .getDir("serializedData", Context.MODE_PRIVATE);
        final File outDir = new File(root.getAbsolutePath() + File.separator
                + "APP Files");
        try {
            final File outputFile = new File(outDir, fileName);
            String data = IOUtils.toString(new FileReader(outputFile));
            LogUtil.d("Files Reader", "Report successfully read as: " + data);
            return data;
        } catch (final IOException e) {
            //e.printStackTrace();
            LogUtil.d("Files Writer",
                    "Unable to read file to external storage");
        }
        return null;
    }

    public static void writePendingSyncData(Context context,
                                            final String fileName, final String data) {

        final File root = context
                .getDir("pendingSyncData", Context.MODE_PRIVATE);
        final File outDir = new File(root.getAbsolutePath() + File.separator
                + "APP Files");
        if (!outDir.isDirectory()) {
            outDir.mkdir();
        }
        try {
            if (!outDir.isDirectory())
                throw new IOException(
                        "Unable to create directory . Maybe the SD card is mounted?");
            final File outputFile = new File(outDir, fileName);
            if (outputFile.exists()) {
                outputFile.delete();
                outputFile.createNewFile();
            }
            Writer writer;
            writer = new BufferedWriter(new FileWriter(outputFile));
            writer.write(data);
            LogUtil.d("Files Writer", "Report successfully saved to: "
                    + outputFile.getAbsolutePath());
            writer.close();
        } catch (final IOException e) {
            e.printStackTrace();
            LogUtil.d("Files Writer",
                    "Unable to write file to external storage");
        }
    }

    public static String readPendingSyncData(Context context,
                                             final String fileName) {
        final File root = context
                .getDir("pendingSyncData", Context.MODE_PRIVATE);
        final File outDir = new File(root.getAbsolutePath() + File.separator
                + "APP Files");
        try {
            final File outputFile = new File(outDir, fileName);
            String data = IOUtils.toString(new FileReader(outputFile));
            LogUtil.d("Files Reader", "Report successfully read as: " + data);
            return data;
        } catch (final IOException e) {
            //e.printStackTrace();
            LogUtil.d("Files Writer",
                    "Unable to read file to external storage");
        }
        return null;
    }

    public static byte[] getFileData(String filePath) {
        byte[] data = null;
        try {
            FileInputStream is = new FileInputStream(new File(filePath));
            data = new byte[is.available()];
            is.read(data, 0, data.length);
            is.close();
        } catch (Exception ex) {
        }

        return data;
    }

    public static FileInputStream getFileDataStream(String filePath) {
        FileInputStream is = null;
        try {
            is = new FileInputStream(new File(filePath));

        } catch (Exception ex) {
        }

        return is;
    }




}