package com.nasaspaceapps.saturn.dto;

/**
 * Created by AhmedSalem on 4/12/15.
 */
public class Crops {

   /* 1 Wheat
    2 Corn
    3 Potatoes
    4 Rice
    5 Tomato
    6 Apple
    7 Onion
    8 Tobacco
    9 Tea
    10 Cocoa */

    String id;

    double suffPercentage = 0;

    public double getSuffPercentage() {
        return suffPercentage;
    }

    public void setSuffPercentage(String suffPercentage) {
        this.suffPercentage = Double.parseDouble(suffPercentage);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
