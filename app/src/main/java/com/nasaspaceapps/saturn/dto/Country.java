package com.nasaspaceapps.saturn.dto;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polygon;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by Salem on 11-Apr-15.
 */
public class Country {
    String name;
    Vector<LatLng> cooardinates;
    LatLng placemark;
    Polygon area;
    Marker marker;

    ArrayList<Crops> crops = new ArrayList<>();

    ArrayList<Crops> idealCrops = new ArrayList<>();


    public Country() {

    }

    public LatLng getPlacemark() {
        return placemark;
    }

    public Marker getMarker() {
        return marker;
    }

    public Polygon getArea() {
        return area;
    }

    public String getName() {
        return name;
    }

    public Vector<LatLng> getCooardinates() {
        return cooardinates;
    }

    public void setCooardinates(Vector<LatLng> cooardinates) {
        this.cooardinates = cooardinates;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlacemark(LatLng placemark) {
        this.placemark = placemark;
    }

    public void setArea(Polygon area) {
        this.area = area;
    }

    public ArrayList<Crops> getCrops() {
        return crops;
    }

    public void setCrops(ArrayList<Crops> crops) {
        this.crops = crops;
    }

    public ArrayList<Crops> getIdealCrops() {
        return idealCrops;
    }

    public void setIdealCrops(ArrayList<Crops> idealCrops) {
        this.idealCrops = idealCrops;
    }
}
