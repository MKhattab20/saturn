package com.nasaspaceapps.saturn.dto;

/**
 * Created by AhmedSalem on 4/12/15.
 */
public class Country_JSON {

    //{"id":"9","country_id":"2","crop_id":"4","value":"1.06422","current":"1"}

    String id;

    String country_id;

    String crop_id;

    String value;

    String current;

    public String getCountry_id() {
        return country_id;
    }

    public String getCrop_id() {
        return crop_id;
    }

    public String getCurrent() {
        return current;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}
