package com.nasaspaceapps.saturn.fragments;


import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.nasaspaceapps.saturn.R;
import com.nasaspaceapps.saturn.dto.Country;
import com.nasaspaceapps.saturn.tasks.LoadPath;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MapsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapsFragment extends android.support.v4.app.Fragment implements GoogleMap.OnMarkerClickListener {


    LoadPath loadPathTask;

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapsFragment newInstance() {
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MapsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link com.google.android.gms.maps.SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            // if (( getActivity().getSupportFragmentManager().findFragmentById(R.id.map)) != null)
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        //mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
        // Getting GoogleMap object from the fragment
        // Zoom in the Google Map
        mMap.animateCamera(CameraUpdateFactory.zoomTo(1));

        mMap.setOnMarkerClickListener(this);
        loadPathTask = new LoadPath(mMap, getActivity());
        loadPathTask.execute("countries_min.kml");
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Country country = loadPathTask.getCountryByTitle(marker.getTitle());

        new AlertDialog.Builder(getActivity()).setMessage(country.getName()).show();
        return false;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpMapIfNeeded();
    }


    Spinner groupsSpinner;
    Spinner cropsSpinner;
    CheckBox checkBox;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_maps, container, false);

        groupsSpinner = (Spinner) v.findViewById(R.id.groupsSpinner);
        cropsSpinner = (Spinner) v.findViewById(R.id.cropsSpinner);

        checkBox = (CheckBox) v.findViewById(R.id.solution);

        groupsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(
                                getActivity(), R.array.Crops_A, android.R.layout.simple_spinner_item);
                        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        cropsSpinner.setAdapter(adapter1);
                        break;
                    case 1:
                        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(
                                getActivity(), R.array.Crops_B, android.R.layout.simple_spinner_item);
                        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        cropsSpinner.setAdapter(adapter2);
                        break;
                    case 2:
                        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(
                                getActivity(), R.array.Crops_C, android.R.layout.simple_spinner_item);
                        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        cropsSpinner.setAdapter(adapter3);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });


        cropsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                isSoution = checkBox.isChecked();
                switch (position) {
                    case 0:
                        switch (groupsSpinner.getSelectedItemPosition()) {
                            case 0:
                                selectedCropID = 2;
                                break;
                            case 1:
                                selectedCropID = 5;
                                break;
                            case 2:
                                selectedCropID = 8;
                                break;
                        }
                        break;
                    case 1:
                        switch (groupsSpinner.getSelectedItemPosition()) {
                            case 0:
                                selectedCropID = 1;
                                break;
                            case 1:
                                selectedCropID = 6;
                                break;
                            case 2:
                                selectedCropID = 9;
                                break;
                        }
                        break;
                    case 2:
                        switch (groupsSpinner.getSelectedItemPosition()) {
                            case 0:
                                selectedCropID = 3;
                                break;
                            case 1:
                                selectedCropID = 7;
                                break;
                            case 2:
                                selectedCropID = 10;
                                break;
                        }
                        break;
                    case 3:
                        selectedCropID = 4;
                        break;
                }

                loadPathTask.redrawMapAreas();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isSoution = isChecked;
                loadPathTask.redrawMapAreas();
            }
        });

        return v;
    }

    public static int selectedCropID = 0;
    public static boolean isSoution = false;
}



