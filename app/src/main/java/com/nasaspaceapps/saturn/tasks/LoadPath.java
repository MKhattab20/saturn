package com.nasaspaceapps.saturn.tasks;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nasaspaceapps.saturn.R;
import com.nasaspaceapps.saturn.dto.Country;
import com.nasaspaceapps.saturn.dto.Country_JSON;
import com.nasaspaceapps.saturn.dto.Crops;
import com.nasaspaceapps.saturn.fragments.MapsFragment;
import com.nasaspaceapps.saturn.util.LogUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class LoadPath extends AsyncTask<String, Void, Void> {


    ArrayList<Country> countries;

    private GoogleMap mMap;
    Activity context;


    public LoadPath(GoogleMap mMap, Activity context) {
        this.mMap = mMap;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (countries == null)
            countries = new ArrayList<>();
    }

    @Override
    protected Void doInBackground(final String... params) {

        try {

            if (countries != null && !countries.isEmpty())
                return null;

            RequestQueue queue = Volley.newRequestQueue(context);
            String url = "http://10.2.157.75/NASA/Saturn_Github/Saturn/index.php/Sufficiency/world_ideal_sufficiency/format/json";
            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            try {
                                createMap(params[0]);


                                TypeToken<List<Country_JSON>> token = new TypeToken<List<Country_JSON>>() {
                                };
                                List<Country_JSON> countries_json = new Gson().fromJson(s, token.getType());

                                for (Country_JSON country_json :
                                        countries_json) {

                                    Country country = getcountryByID(country_json.getCountry_id());


                                    Crops crops = new Crops();
                                    crops.setId(country_json.getCrop_id());
                                    crops.setSuffPercentage(country_json.getValue());
                                    country.getIdealCrops().add(crops);

                                }

                                context.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        redrawMapAreas();
                                    }
                                });

                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (ParserConfigurationException e) {
                                e.printStackTrace();
                            } catch (SAXException e) {
                                e.printStackTrace();
                            }

                            RequestQueue queue = Volley.newRequestQueue(context);
                            String url = "http://10.2.157.75/NASA/Saturn_Github/Saturn/index.php/Sufficiency/world_current_sufficiency/format/json";
                            // Request a string response from the provided URL.
                            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            // Display the first 500 characters of the response string.
                                            LogUtil.i("TAG", "Response is: " + response.substring(0, 500));


                                            try {

                                                TypeToken<List<Country_JSON>> token = new TypeToken<List<Country_JSON>>() {
                                                };
                                                List<Country_JSON> countries_json = new Gson().fromJson(response, token.getType());

                                                for (Country_JSON country_json :
                                                        countries_json) {

                                                    Country country = getcountryByID(country_json.getCountry_id());


                                                    Crops crops = new Crops();
                                                    crops.setId(country_json.getCrop_id());
                                                    crops.setSuffPercentage(country_json.getValue());
                                                    country.getCrops().add(crops);

                                                }

                                                context.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        redrawMapAreas();
                                                    }
                                                });
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                                    //mTextView.setText("That didn't work!");
                                }
                            });

                            // Add the request to the RequestQueue.
                            queue.add(stringRequest);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                }
            });

            queue.add(stringRequest);

            return null;


        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private Country getcountryByID(String country_id) {
        int id = Integer.parseInt(country_id);

        switch (id) {
            case 1:
                return getCountryByTitle("Egypt");
            case 3:
                return getCountryByTitle("China");
            case 2:
                return getCountryByTitle("Brazil");
            case 4:
                return getCountryByTitle("Italy");
            case 5:
                return getCountryByTitle("Mexico");
        }

        return null;
    }

    private boolean createMap(String param) throws
            IOException, ParserConfigurationException, SAXException {
        InputStream inputStream = context.getAssets().open(param);


        DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        Document document = docBuilder.parse(inputStream);

        NodeList listCoordinateTag = null;

        if (document == null) {
            return true;
        }


        listCoordinateTag = document.getElementsByTagName("S_country");
        for (int i = 0; i < listCoordinateTag.getLength(); i++) {
            Country country = new Country();
            Node countryNode = listCoordinateTag.item(i);
            // country.cooardinates = new Vector<>();

            NodeList countriesNodelist = countryNode.getChildNodes();

            for (int k = 0; k < countriesNodelist.getLength(); k++) {
                Node node = countriesNodelist.item(k);
                if ((node.getLocalName() != null && node.getLocalName().equals("name")) || (node.getNodeName() != null && node.getNodeName().equals("name"))) {
                    country.setName(node.getFirstChild().getNodeValue().trim());
                }
                if ((node.getLocalName() != null && node.getLocalName().equals("Point")) || (node.getNodeName() != null && node.getNodeName().equals("Point"))) {
                    String coordText = node.getChildNodes().item(1).getFirstChild().getNodeValue().trim();
                    country.setPlacemark(new LatLng(Double.parseDouble(coordText.split("\\,")[1]), Double.parseDouble(coordText.split("\\,")[0])));
                }
                if ((node.getLocalName() != null && node.getLocalName().equals("MultiGeometry")) || (node.getNodeName() != null && node.getNodeName().equals("MultiGeometry"))) {
                    NodeList multiNodelist = node.getChildNodes();

                    for (int m = 0; m < multiNodelist.getLength(); m++) {
                        Node geometryNode = multiNodelist.item(m);
                        if ((geometryNode.getLocalName() != null && geometryNode.getLocalName().equals("Polygon")) || (geometryNode.getNodeName() != null && geometryNode.getNodeName().equals("Polygon"))) {
                            String coordText = geometryNode.getChildNodes().item(1).getChildNodes().item(1).getChildNodes().item(1).getFirstChild().getNodeValue().trim();

                            String[] vett = coordText.split("\\ ");

                            Vector<LatLng> temp = new Vector<>();
                            for (String vect : vett) {
                                temp.add(new LatLng(Double.parseDouble(vect.split("\\,")[1]), Double.parseDouble(vect.split("\\,")[0])));
                            }

                            if (country.getCooardinates() == null || country.getCooardinates().isEmpty() || country.getCooardinates().size() < temp.size())
                                country.setCooardinates(temp);
                        }
                    }


                }
            }
            countries.add(country);
        }
        return false;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        redrawMapAreas();


    }

    public void redrawMapAreas() {
        mMap.clear();
        for (int i = 0; i < countries.size(); i++) {

            Country country = countries.get(i);
            LogUtil.i("TAG", "country number " + i);


            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(country.getPlacemark());
            markerOptions.title(country.getName());
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin));


            country.setMarker(mMap.addMarker(markerOptions));


            PolygonOptions temp2 = new PolygonOptions();

            for (LatLng latLng : country.getCooardinates()) {
                temp2.add(latLng);
            }

            Polygon area = mMap.addPolygon(temp2);

            area.setStrokeWidth(0);


            Crops crops = getcropByID(country, MapsFragment.selectedCropID);

            double percentage;

            if (crops == null) {
                percentage = 0;
            } else {
                percentage = crops.getSuffPercentage();
            }

            int fillColor;
            if (percentage <= 0.5) {
                fillColor = Color.argb((int) (200 - percentage * 400), 170, 0, 0);
            } else if (percentage <= 1) {
                fillColor = Color.argb((int) ((percentage - 0.5) * 400), 0, 0, 170);
            } else {
                fillColor = Color.argb(200, 0, 170, 0);
            }
            area.setFillColor(fillColor);

            area.setGeodesic(true);
            area.setVisible(true);


            country.setArea(area);
        }
    }

    private Crops getcropByID(Country country, int selectedCropID) {

        if (MapsFragment.isSoution) {
            for (Crops crops : country.getIdealCrops()) {
                if (crops.getId().equalsIgnoreCase(selectedCropID + ""))
                    return crops;
            }
        } else {
            for (Crops crops : country.getCrops()) {
                if (crops.getId().equalsIgnoreCase(selectedCropID + ""))
                    return crops;
            }
        }
        return null;
    }


    public Country getCountryByTitle(String title) {

        for (Country country : getCountries()) {
            if (country.getName().equalsIgnoreCase(title))
                return country;
        }
        return null;
    }

    public ArrayList<Country> getCountries() {
        return countries;
    }
}